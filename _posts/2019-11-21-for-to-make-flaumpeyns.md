---
layout: post
title: For to make flaumpeyns
subtitle: compiled of the chef Maister Cokes of kyng Richard the Secunde kyng of Englond aftir the Conquest
tags: [recipes, middle english]
cover-img: /assets/img/ingredients.jpg
---

Take clene pork and boile it tendre. þenne hewe it small and bray it
smal in a morter. take fyges and boile hem tendre in smale ale. and
bray hem and tendre chese þerwith. þenne waisthe hem in water & þene
lyes[^1] hem alle togider wit Ayrenn, þenne take powdour of pepper.
or els powdour marchannt & ayrenn and a porcioun of safroun and salt.
þenne take blank sugur. eyrenn & flour & make a past wit a roller,
þene make þerof smale pelettes[^2]. & fry hem broun in clene grece &
set hem asyde. þenne make of þat ooþer deel[^3] of þat past long
coffyns[^4] & do þat comade[^5] þerin. and close hem faire with a
countoer[^6], & pynche hem smale about. þanne kyt aboue foure oþer
sex wayes, þanne take euy[^7] of þat kuttyng up, & þenne colour it
wit zolkes of Ayrenn, and plannt hem thick, into the flaumpeyns above
þat þou kuttest hem & set hem in an ovene and lat hem bake eselich[^8].
and þanne serue hem forth.

Source: [*Samuel Pegge. The Forme of Cury: A Roll of Ancient English Cookery Compiled, about A.D. 1390.*](https://www.gutenberg.org/ebooks/8102) \
Image: [*Colleen Galvin. Food for medieval meal. CC-BY-2.0.*](https://flic.kr/p/c25o7N)

[^1]: lyer. mix.
[^2]: Pelettes. *Pelotys* Ms. Ed. No. 16. Balls, pellets, from Fr. *pelote*.
[^3]: deel. deal, i.e. part, half.
[^4]: Coffyns. Pies without lids.
[^5]: comade. Qu.
[^6]: coutour. coverture, a lid.
[^7]: euy. every.
[^8]: eselich. easily, gently.
