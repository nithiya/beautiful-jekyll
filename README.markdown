# beautiful-jekyll

Beautiful Jekyll theme demo (v6.0.1) for GitLab Pages: <https://nithiya.gitlab.io/beautiful-jekyll>

Repository: <https://gitlab.com/nithiya/beautiful-jekyll>

Includes the following:

- sitemap
- Lunr.js search function
- comments with Staticman

## Serving this site locally

1.  Install Ruby.

2.  Clone this repository:

    Option 1 - SSH:

    ```sh
    git clone git@gitlab.com:nithiya/beautiful-jekyll.git
    ```

    Option 2 - HTTPS:

    ```sh
    git clone https://gitlab.com/nithiya/beautiful-jekyll.git
    ```

3.  Navigate to the cloned directory and install all requirements:

    ```sh
    cd beautiful-jekyll
    gem install bundler
    bundle install
    ```

4.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

5.  View the site at `http://localhost:4000/beautiful-jekyll/`.

## License

[MIT License](https://opensource.org/licenses/MIT)

This repository is maintained by Nithiya Streethran (nmstreethran at gmail dot com)

## Credits

- [Beautiful Jekyll](https://beautifuljekyll.com) by [Dean Attali](https://deanattali.com)
- [Jekyll](https://jekyllrb.com/)
- [Ruby programming language](https://www.ruby-lang.org/en/)
- [Lunr.js](https://lunrjs.com/)
- [Jekyll Codex](https://jekyllcodex.org)
- [Staticman](https://staticman.net/)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Shields.io](https://shields.io/)
- [Project Gutenberg](https://www.gutenberg.org/)
- [reCAPTCHA](https://www.google.com/recaptcha/about/)
- [Fly](https://fly.io/)
- [Akismet](https://akismet.com)

## References

- [Beautiful Jekyll GitHub repo](https://github.com/daattali/beautiful-jekyll)
- [Beautiful Jekyll site - Ruby gem installation steps](https://beautifuljekyll.com/getstarted/#install-steps-hard)
- Bundler issues - [Arch Linux forum](https://bbs.archlinux.org/viewtopic.php?id=265534) / [Jekyll Talk](https://talk.jekyllrb.com/t/error-no-implicit-conversion-of-hash-into-integer/5890)
- [Configuration options - Jekyll](https://jekyllrb.com/docs/configuration/options/)
- [Search with Lunr.js - Jekyll Codex](https://jekyllcodex.org/without-plugin/search-lunr/)
- [Staticman comments set-up - Nithiya Streethran](https://nithiya.gitlab.io/post/staticman-jekyll-gitlab/)
